// Copyright (c) 2016 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package datastore

import (
	"appengine"
	gaedatastore "appengine/datastore"
	"appengine/memcache"
	"fmt"
	"github.com/gin-gonic/gin"
)

type Datastore struct {
	c appengine.Context
}

func NewDatastore(c appengine.Context) *Datastore {
	return &Datastore{c: c}
}

func DatastoreHandler() gin.HandlerFunc {
	return func(g *gin.Context) {
		var c appengine.Context
		v, ok := g.Get("context")
		if ok {
			c, ok = v.(appengine.Context)
		}
		if !ok {
			c = appengine.NewContext(g.Request)
		}
		g.Set("datastore", NewDatastore(c))
	}
}

func (ds *Datastore) Load(kind string, id int64, entity interface{}) (int64, error) {
	if id == 0 {
		return 0, fmt.Errorf("gingaedatastore: invalid id: %v", id)
	}

	// Get entity key from specified id.
	k := gaedatastore.NewKey(ds.c, kind, "", id, nil)

	// Retrieve data.
	if err := gaedatastore.Get(ds.c, k, entity); err != nil {
		return 0, fmt.Errorf("gingaedatastore: error retrieving data: %v", err.Error())
	}
	return k.IntID(), nil
}

func (ds *Datastore) Save(kind string, id int64, entity interface{}) (int64, error) {
	// Get entity key from id.
	k := gaedatastore.NewKey(ds.c, kind, "", id, nil)

	// Store data.
	k, err := gaedatastore.Put(ds.c, k, entity)
	if err != nil {
		return 0, fmt.Errorf("gingaedatastore: error storing data: %v", err.Error())
	}
	return k.IntID(), nil
}

func (ds *Datastore) NewQuery(kind string) *gaedatastore.Query {
	return gaedatastore.NewQuery(kind)
}

func (ds *Datastore) Run(q *gaedatastore.Query, cursor string, callback func(*gaedatastore.Iterator)) {
	// Attempt to retrieve cursor from memcache if specified.
	if cursor != "" {
		item, err := memcache.Get(ds.c, cursor)
		if err == nil {
			c, err := gaedatastore.DecodeCursor(string(item.Value))
			if err == nil {
				q = q.Start(c)
			}
		}
	}

	// Run query and callback with iterator.
	t := q.Run(ds.c)
	callback(t)

	// Store cursor in memcache if specified.
	if cursor != "" {
		if c, err := t.Cursor(); err == nil {
			memcache.Set(ds.c, &memcache.Item{Key: cursor, Value: []byte(c.String())})
		}
	}
}
